<?php

  include 'conn.php';

  $sql = "SELECT * FROM user";
  $result = mysqli_query($conn, $sql);

  if (mysqli_num_rows($result) > 0) {

    echo "<table>";
      echo "<tr>";
        echo "<th>Id</th/>";
        echo "<th>Name</th>";
        echo "<th>Father's Name</th>";
        echo "<th>Mother's Name</th>";
        echo "<th>DOB</th>";
        echo "<th>Nation</th>";
        echo "<th>Address</th>";
        echo "<th>Ph Number</th>";
      echo "</tr>";

      // output data of each row
      while($row = mysqli_fetch_assoc($result)) {

        echo "<tr>";
          echo "<td>" . $row["id"]."</td>";
          echo "<td>" . $row["name"]. "</td>";
          echo "<td>" . $row["father_name"]."</td>";
          echo "<td>" . $row["mother_name"] ."</td>";
          echo "<td>" . $row["dob"] ."</td>";
          echo "<td>" . $row["email"] . "</td>";
          echo "<td>" . $row["nation"] ."</td>";
          echo "<td>" . $row["address"] ."</td>";
          echo "<td>" . $row["ph_number"] ."</td>";
        echo "</tr>";
      }
    echo "</table>";
  } else {
      echo "0 results";
  }
?>
