<html>

  <head>
    <style>

      body{
        margin: 0px;
        background-image: url("background3.jpg");
        background-repeat: no-repeat;
        background-size: 100% 720px;
      }


      .header{
        border:1px solid red;
        padding-left: 15px;
        background-color: rgba(52, 73, 94, 0.7);
      }

      .logout-btn{
        margin-left:92%;
        padding: 15px 30px;
        color: #fff;
        border-radius: 4px;
        border: none;
        background-color: #2ECC71;
        border-bottom: 4px solid #27AE60;
      }

      .options{
        position: fixed;
        margin-left: 0px;
        margin-top: 10px;
        padding-left: 10px;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 50px;
        height: auto;
        width: 15%;
        height: 80%;

        background-color: rgba(52, 73, 94, 0.7);
      }

      .options ul li{
        padding: 10px;
        list-style-type: none;
      }

      .options ul li a{
        color: #fff;
      }

      .controls{
        float:right;
        margin-left: 22%;
        width: 1050px;
        height: 0 auto;
      }

    </style>

  <body>
     <div style="" class="header">
       <?php
         session_start();

         echo "<h2 style='color:#fff;'>Welcome <u><b>" . $_SESSION['username'] . "</b></u> to admin control panel</h2>";

        ?>
       <form action="destroySession.php">
         <input type="submit" value="Logout" class="logout-btn">
       </form>

     </div>

     <div class="bodyControl" style="margin: 0 auto;position:relative">
       <div class="options">
         <center><h2 style="color:#fff;"><u><b>Controls</b></u></h2></center>
         <ul>
           <li><b> <a href="display_table.html" target="table"><font> Display/Search Table </font></a></b></li>
           <li><b> <a href="insert_data.html" target="table"><font> Insert Data </font></a></b></li>
           <li><b> <a href="" target="table"><font> Update Table </font></a></b></li>
           <li><b> <a href="del.html" target="table"><font> Delete </font></a></b></li>

       </div>

       <div class="controls">
         <iframe height="400px" width="100%" src="display.html" name="table" style="border:none;"></iframe>
       </div>
     </div>



 </body>
</html>
