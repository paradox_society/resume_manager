<!DOCTYPE html>
<html manifest="cache.appcache" lang="en">

<head>

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

     <!-- icon: http://fontawesome.io/icon/angle-double-down/ -->

    <style>
        body,
        html {
            height: 100%;
            margin: 0;
            font: 400 15px/1.8 "Lato", sans-serif;
            color: #777;
        }

        .caption {
            position: absolute;
            left: 0;
            top: 17%;
            width: 100%;
            text-align: center;
            color: #000;
        }

        .caption span.border {
            background-color: #111;
            color: #fff;
            padding: 18px;
            font-size: 25px;
            letter-spacing: 10px;
        }

        .cvAndresume {
            font-size: 20px;
            color: #000;
            margin-left: 26%;
            margin-right: 30%;
            table.equalDevide tr td {
            width: 25%;
            }
          }


          /*Templet img captions */
          .tempImg .text{
            position:relative;
            bottom:30px;
            left:0px;
            visibility:hidden;
          }


          .tempImg:hover {
              box-shadow: 0px 0px 150px #000000;
              z-index: 2;
              -webkit-transition: all 200ms ease-in;
              -webkit-transform: scale(1.5);
              -ms-transition: all 200ms ease-in;
              -ms-transform: scale(1.5);
              -moz-transition: all 200ms ease-in;
              -moz-transform: scale(1.5);
              transition: all 200ms ease-in;
              transform: scale(3);

              .text.visibility: visible;
            }


            /* Model BOX CSS */

            .modal-container{
              position: fixed;
              background-color: #fff;
              border: 1px solid #000;
              left: 50%;
              padding: 20px;
              border-radius: 5px;


              -webkit-transform: translate(,-50%,0);
              -ms-transform: translate(,-50%,0);
              transform: translate(,-50%,0);
            }

            /* Model BOX CSS ^ */

        div {
            display: block;
        }

        .backImage1,
        .backImage2,
        .backImage3 {
            position: relative;
            opacity: 0.65;
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .backImage1 {
            background-image: url("img_parallax.jpg");
            min-height: 100%;
        }

        .backImage2 {
            background-image: url("img_parallax2.jpg");
            min-height: 100%;
        }

        .backImage3 {
            background-image: url("http://www.w3schools.com/howto/img_parallax3.jpg");
            min-height: 100%;
        }

        footer {
            background: #aaa;
            color: white;
            height: 5%;
            text-align: center;
        }
        /* FORM FILL UP CSS */

        table input {
            width: 100%;
            font-size: 15px;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 10px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
            transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
            border: none;
        }

        table label {
            font-size: 15px;
        }

        .sub,
        .res {
            width: 90px;
            height: 50px;
        }

        .divForm {
            height: 50%;
            width: 50%;
            margin-left: 28%;
            margin-right: 28%;
            color: black;
        }
        /* Arrow */

        .icoChild{
             width: 70px;
             margin: 0px auto;
             -webkit-animation: bounce 2s infinite;
             animation: bounce 2s infinite;
        }

        .icoParent{
             position: absolute;
             bottom: 0px;
             width: 100%;
             cursor: pointer;
        }

        .icoChild:hover{
          -webkit-animation: paused;
          animation: paused;
        }
        .members{
          margin-bottom: 30px;
        }

        @-webkit-keyframes icoChild {
             0%,
             20%,
             50%,
             80%,
             100% {
                  -webkit-transform: translateY(0);
                  transform: translateY(0);
             }
             40% {
                  -webkit-transform: translateY(-30px);
                  transform: translateY(-30px);
             }
             60% {
                  -webkit-transform: translateY(-15px);
                  transform: translateY(-15px);
             }
        }
              0%,
             20%,
             50%,
             80%,
             100% {
                  -webkit-transform: translateY(0);
                  transform: translateY(0);
             }
             40% {
                  -webkit-transform: translateY(-30px);
                  transform: translateY(-30px);
             }
             60% {
                  -webkit-transform: translateY(-15px);
                  transform: translateY(-15px);
             }
        }

        .devs p{
          display:inline-block;
          padding-left: 5%;
          padding-right: 5%;
          padding-top: 2%;
        }

        .devs font{
          word-wrap: break-word;
          float: right;
        }

        .dropdown-content{
          display: none;
        }

        /*modal css style Here... */

        /* The Modal (background) */
        .modal {
          display: none; /* Hidden by default */
          position: fixed;
          z-index: 1; /* Sit on top */
          padding-top: 100px; /* Location of the box */
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
          }

          /* Modal Content */
          .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
          }

          /* The Close Button */
          .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
          }

          .close:hover,
          .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
          }

          .wrap: hover {
            color: #000;
          }

          ::-webkit-scrollbar {
            display: none;
          }


    </style>
</head>

<body>



    <div class="backImage1">
        <div class="caption">
            <span class="border">Resume Manager</span>
        </div>


        <div class="icoParent">
             <div class="icoChild text-center" onClick="document.location='#ComeHereBaby';">
               <i class="fa fa-angle-double-down fa-4x center-block" aria-hidden="true" style="color:white;text-align:center"></i>
             </div>
        </div>

    </div>

    <div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;" id = "ComeHereBaby" name="ComeHereBaby">
        <h3 style="text-align:center;">Speality about <b>Resume Manager</b></h3>
        <p class="text-center">No Signup or Sign and guess what, we are absulutely Free. You can also mail us if you need more feilds to add to yout CV or Resume...</p>
    </div>

    <div class="backImage2">
        <div class="caption">
            <span class="border" style="background-color:transparent;font-size:25px;color: #f7f7f7;">Our Templetes</br>
            </span>
               <table class="cvAndresume" cellpadding="0" cellspacing="0" width="50%">
                    <tr class="tableHeading" style="border:5px solid;">
                         <td colspan="3"><b><font face="Courier New">Templates</font></b></td>
                    </tr>
                    <tr>
                         <div class="tempImg" style="margin-top:4%;">
                           <td><img src="temp1.jpg" class="tempImg" id = "modalImg" width = "50%" style = "height:auto;margin-top:10%;"></td>

                           <td><img src="temp2.jpg" class="tempImg" id = "modalImg1" width = "90%" style = "height:auto;margin-top:10%;"></td>

                           <td><img src="temp3.jpg" class="tempImg" id = "modalImg2" width = "60%" style = "height:auto;margin-top:10%;"></td>

                           <p class="text"><center><b>Click To Fill Form</b></center></p>

                         </div>
                    </tr>
               </table>
        </div>
    </div>

<!-- Trigger/Open The YoYo MOdal-->

<!-- The modal-->
  <div id = "myModal" class="modal">
    <!-- Modal content START -->

    <div class = "modal-content">
      <span class = "close">&times;</span>
      <p><font style =" font-size:20px;color:#000;">Fill up this details: </font></p>
      <div class="">
                  <div class="divForm">
                    <table class="mainTable">
                      <form action="/php/insertdata.php" method = "post" onsubmit = "return(verify());" >
                      <tr>
                      <td>
                        <label>Name:<font color = "red">*</font></label>
                      </td>
                        <td><input type = "text" name = "name" id = "name" class = "inpBox" ></td>
                      </tr>
                      <tr>
                        <td>
                          <label>Father's Name:<font color="red">*</font></label>
                        </td>
                        <td><input type = "text" name = "fathername" id = "fathername" class = "inpBox">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label>Mother's Name:<font color = "red">*</font></label>
                        </td>
                        <td>
                          <input type="text" name = "mothername" id = "mothername" class = "inpBox" >
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label>Date of Birth (dd/mm/yyyy)<font color="red">*</font></label>
                        </td>
                        <td>
                          <select class="DOBDays" name="dobDay">
                            <option> - Day - </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>
                          <select class="DOBMonth" name="dobMonth">
                            <option> - Month - </option>
                            <option value="01">Jan</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Apr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Aug</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dec</option>
                          </select>
                          <select class="DOBYear" name="dobYear">
                            <option> - Year - </option>
                            <option value="1993">1993</option>
                            <option value="1992">1992</option>
                            <option value="1991">1991</option>
                            <option value="1990">1990</option>
                            <option value="1989">1989</option>
                            <option value="1988">1988</option>
                            <option value="1987">1987</option>
                            <option value="1986">1986</option>
                            <option value="1985">1985</option>
                            <option value="1984">1984</option>
                            <option value="1983">1983</option>
                            <option value="1982">1982</option>
                            <option value="1981">1981</option>
                            <option value="1980">1980</option>
                            <option value="1979">1979</option>
                            <option value="1978">1978</option>
                            <option value="1977">1977</option>
                            <option value="1976">1976</option>
                            <option value="1975">1975</option>
                            <option value="1974">1974</option>
                            <option value="1973">1973</option>
                            <option value="1972">1972</option>
                            <option value="1971">1971</option>
                            <option value="1970">1970</option>
                            <option value="1969">1969</option>
                            <option value="1968">1968</option>
                            <option value="1967">1967</option>
                            <option value="1966">1966</option>
                            <option value="1965">1965</option>
                            <option value="1964">1964</option>
                            <option value="1963">1963</option>
                            <option value="1962">1962</option>
                            <option value="1961">1961</option>
                            <option value="1960">1960</option>
                            <option value="1959">1959</option>
                            <option value="1958">1958</option>
                            <option value="1957">1957</option>
                            <option value="1956">1956</option>
                            <option value="1955">1955</option>
                            <option value="1954">1954</option>
                            <option value="1953">1953</option>
                            <option value="1952">1952</option>
                            <option value="1951">1951</option>
                            <option value="1950">1950</option>
                            <option value="1949">1949</option>
                            <option value="1948">1948</option>
                            <option value="1947">1947</option>
                          </select>
                        </td>
                      </tr>	<tr>
                        <td>
                          <label>Email id: <font color = "red">*</font></label>
                        </td>
                        <td>
                          <input type = "text" name = "email" id = "email" class = "inpBox" onblur = "validateEmail(this);">
                        </td>
                      </tr>	<tr>
                        <td>
                          <label>Nationality:<font color = "red">*</font></label>
                        </td>
                        <td>
                          <input type = "text" name = "nat" id = "nat" >
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <table class="education">

                            <tr>
                              <th>Degree</th>
                              <th>Institute/University</th>
                              <th>Year</th>
                              <th>Percentage</th>
                            </tr>
                            <tr>
                              <td>
                                <select name="deg">
                                  <option>Select Degree...</option>
                                  <option value="UG">Bachelors Degree</option>
                                  <option value="PG">Master's Degree</option>
                                  <option value="Phd">phD</option>
                                  <option value="other">Any other</option>
                                </select>
                              </td>

                              <td>
                                <input type="text" placeholder="University" name="univer">
                              </td>

                              <td>
                                <input type="number" placeholder="Percentage" name="year">
                              </td>
                              <td>
                                <input type="number" placeholder="Percentage" name="score">
                              </td>
                            </tr>

                          }
                            <tr>
                              <td>
                                <select>
                                <option>Select Degree...</option>
                                <option>Bachelors Degree</option>
                                <option>Master's Degree</option>
                                <option>phD</option>
                                <option>Any other</option>
                              </select>
                              </td>

                              <td>
                                <input type="text" placeholder="University" name="univer">
                              </td>

                              <td>
                                <input type="text" placeholder="Passout year" name="py">
                              </td>
                              <td>
                                   <input type="number" placeholder="Percentage" name="score">
                              </td>                            </tr>
                            <tr>
                              <td>
                                <select>
                                <option>Select Degree...</option>
                                <option>Bachelors Degree</option>
                                <option>Master's Degree</option>
                                <option>phD</option>
                                <option>Any other</option>
                              </select>
                              </td>

                              <td>
                                <input type="text" placeholder="University" name="univer">
                              </td>

                              <td>
                                <input type="text" placeholder="Passout year" name="py">
                              </td>
                              <td>
                                   <input type="number" placeholder="Percentage" name="score">
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <label>Address for communication<font color = "red">*</font></label>
                        </td>
                        <td>
                          <textarea name="address"></textarea>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label>Phone Number:<font color="red">*</font></label>
                        </td>
                        <td>
                          <input type = "number" id = "pno" name = "pno" class = "inpBox" >
                        </td>
                      </tr>

                      <tr>
                        <td colspan="2">
                          <center>
                            <input type="submit" value="Create PDF" class="sub">
                            <input type="reset" value="Reset" class="res">
                          </center>
                        </td>

                      </tr>
                       </form>
                  </table>
                </div>
        </div>
      </div>
  </div>

    <!-- Modal container or Modal box body END -->

  <!--
    <div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
        <h3 style="text-align:center;">Speality about <b>Resume Manager</b></h3>
        <p class="text-center">No Signup or Sign and guess what, we are absulutely Free. You can also mail us if you need more feilds to add to yout CV or Resume...</p>
    </div>-->

     <div class="members" style="color: #777;background-color:white;text-align:center;padding:10px 80px;text-align: justify;">
        <h3 style="text-align:center;">Members Of Our Team</b></h3>
        <div class="text-center devs">
          <p style="margin-left:20px; margin-right:20px">
            <b>
              <h4>

              <a href="https://in.linkedin.com/in/gourab-dasgupta-009a8458"  style="text-decoration:none">
                <div class="wrap">
                  <font style="float:left; padding-left:35%;color:#777;">Gourab Dasgupta</font>
                </div>
              </a>

                <font style="float:right;padding-right:35%;">Suhani Vedi</font>

            </h4>
          </b>
          </p>

          </div>

    </div>

    <footer>Copyright &copy; Resume Manager</footer>

     <!-- JAVASCRIPT ZONE AFTER THIS POINT -->

     <script>

      // Get the yo yo modal
      var modal = document.getElementById('myModal');
      // Get the button that opens the modal
      var img = document.getElementById("modalImg");
      var img1 = document.getElementById("modalImg1");
      var img2 = document.getElementById("modalImg2");


      // Get the <span> element that closes the modal, btw &time is the "X" hehe kool huhh!
      var span = document.getElementsByClassName("close")[0];

      // When the user clicks the button, open the modal


      img.onclick = function() {
          modal.style.display = "block";
      }
      img1.onclick = function() {
          modal.style.display = "block";
      }
      img2.onclick = function() {
          modal.style.display = "block";
      }

      // When the user clicks on <span> (x)<----&time does that, close the modal
      span.onclick = function() {
          modal.style.display = "none";
      }


      // When the user clicks anywhere outside of the modal, close it :P
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }

      /*form Validationn */
      function verify()
      {
        var name = document.getElementById("name");
        var fathername = document.getElementById("fathername");
        var mothername = document.getElementById("mothername");
        var email = document.getElementById("email");
        var nat = document.getElementById("nat");
        var pno = document.getElementById("pno");

        var msg = "";

        if(name.value == ""){
          msg+= "Please enter your name!\n";
          name.className = "inpBoxError";
        }
        if(fathername.value == ""){
          msg+= "Please enter Father's Name!\n";
          fathername.className = "inpBoxError";
        }
        if(mothername.value == ""){
          msg+= "Please enter Mother's Name!\n";
          mothername.className = "inpBoxError";
        }
        if(email.value == ""){
          msg+= "Please enter your email!\n";
          email.className = "inpBoxError";
        }
        if(nat.value == ""){
          msg+= "Please enter your nationality!\n";
          nat.className = "inpBoxError";
        }
        if(pno.value == ""){
          msg+= "Please enter your Mobile Number!";
          pno.className = "inpBoxError";
        }

        if(pno.value == "" || isNaN(pno.value) || pno.value.length != 10){
          msg+= "\nPlease provide a Mobile No in the format 123";
          pno.className="inpBoxError";
        }

        if(msg == ""){
          return true;
        }else {
          alert(msg);
          return false;
        }

      }

      function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false)
        {
            alert('Invalid Email Address');
            return false;
        }

        return true;

      }

     </script>


</body>
</html>
